<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <div class="login100-pic js-tilt" data-tilt>
                    <img src="<?= base_url() ?>ext/images/wifi.png" alt="IMG">
            </div>

            <form class="login100-form formLogin">
                <span class="login100-form-title">
                    Bienvenidos al Portal de Wifi de Plan Vital.
                </span>

                <div class="wrap-input100 validate-input">
                    <input class="input100" type="text" name="cod_user" placeholder="Nombre de Usuario">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
                            <i class="fa fa-user" aria-hidden="true"></i>
                    </span>
                </div>

                <div class="wrap-input100 validate-input">
                    <input class="input100" type="password" name="password" placeholder="Contraseña">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                    </span>
                </div>
                
                <div class="text-right p-t-8">
                    <span class="txt1">
                    </span>
                    <a class="txt1" href="<?= base_url() ?>Welcome/recuperaPass">
                            ¿Has olvidado tu contraseña?
                    </a>
                </div>

                <div class="p-t-12">
                    <span class="txt1">
                        Al hacer click en el inicio de sesion aceptas todas nuestras 
                    </span>
                    <a class="txt2" href="#" data-toggle="modal" data-target="#modalCondiciones">   
                            Condiciones de uso.
                    </a>
                </div>

                <div class="container-login100-form-btn">
                    <button type="submit" class="login100-form-btn">
                            Iniciar Sesion
                    </button>
                </div>
                
                <div class="text-right p-t-8">
                    <span class="txt1">
                        No esta registrado?
                    </span>
                    <a class="txt2" href="<?= base_url() ?>Welcome/registro">
                            Registrar.
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="modalCondiciones" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Condiciones de uso</h4>
            </div>
            <div class="modal-body">
                <!-- Code -->
                <p>
                    What is Lorem Ipsum?

                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Hecho</button>
            </div>
        </div>
    </div>
</div>