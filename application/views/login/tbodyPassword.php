<form class="login100-form">
    <span class="login100-form-title">
        Actualizacion de cuenta realizado correctamente!
    </span>
    <div class="text-center p-t-12">
        <span class="txt1">
            La contraseña se ha enviado a:
        </span>
    </div>
    <div class="text-center p-t-12">
        <span class="txt2">
            <b><?= $email ?></b>
            <br>
            <b><?= $telefono ?></b>
        </span>
    </div>
    <div class="text-center p-t-12">
        <span class="txt1">
            Despues de recuperar sus credenciales, haga click a continuacion para iniciar sesion.
        </span>
    </div>
    <div class="container-login100-form-btn">
        <button class="login100-form-btn iniciarSesion">Iniciar Sesión</button>
    </div>
</form>