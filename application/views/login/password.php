<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <div class="login100-pic js-tilt" data-tilt>
                    <img src="<?= base_url() ?>ext/images/wifi.png" alt="IMG">
            </div>
            <div class="tbodyRegistro">
                <form class="login100-form formPassword">
                    <span class="login100-form-title">
                        Ingrese su E-Mail para recuperar sus credenciales.
                    </span>
                    <div class="wrap-input100 validate-input">
                        <input class="input100" type="text" name="email" placeholder="E-Mail" required pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}">
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                                <i class="fa fa-at" aria-hidden="true"></i>
                        </span>
                    </div>
                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn">
                                Enviar
                        </button>
                    </div>
                    <div class="text-right p-t-8">
                        <span class="txt1">
                            ¿Ya esta registrado?
                        </span>
                        <a class="txt2" href="<?= base_url() ?>Welcome  ">
                                Inicie Sesion.
                        </a>
                    </div>
                    <div class="text-right p-t-8">
                        <span class="txt1">
                            No esta registrado?
                        </span>
                        <a class="txt2" href="<?= base_url() ?>Welcome/registro">
                                Registrar.
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>