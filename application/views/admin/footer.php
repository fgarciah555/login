<!--===============================================================================================-->	
<script src="<?= base_url() ?>ext/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="<?= base_url() ?>ext/vendor/bootstrap/js/popper.js"></script>
<script src="<?= base_url() ?>ext/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="<?= base_url() ?>ext/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<!--===============================================================================================-->
<script src="<?= base_url() ?>ext\vendor\swal\sweetalert2.min.js"></script>
<!--===============================================================================================-->
<script src="<?= base_url() ?>ext/vendor/jqueryRut/jquery.rut.js" type="text/javascript"></script> 
<script>
    
    $("input#rut").rut({
        formatOn: 'keyup',
        minimumLength: 8, // validar largo mínimo; default: 2
        validateOn: 'change' // si no se quiere validar, pasar null
    });

    // es posible pasar varios eventos separados por espacio, útil
    // para validar el rut aún cuando el browser autocomplete el campo
    $("input#rut").rut({validateOn: 'change keyup'});

    // si no se quiere mostrar el punto para separador de miles, 
    // pasar la opción useThousandsSeparator : false
    $("input#rut").rut({useThousandsSeparator : false}); //formateará '145694841' como '14569484-1'
    
//    $("input#rut").rut().on('rutInvalido', function(e) {
//	alert("El rut " + $(this).val() + " es inválido");
//    });

    $(document).on('submit','.formLogin',function(event){
        event.preventDefault();
        $.ajax({
            url: '<?= base_url() ?>Welcome/login',
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'JSON'
        }).done(function (data) {
            console.log(data);
            if(data.status){
                window.location.href = 'https://planvital.cl/affiliate/home';
                console.log('Ogey');
            }else{
                Swal.fire(
                    'Ocurrio un Problema',
                    data.message,
                    'error'
                );
                console.log('RRAT');
            }
        }).fail(function (data) {
            console.log(data);
        });
    });
    
    $(document).on('submit','.formRegistro',function(event){
        event.preventDefault();
        var result = $('#rut').val().split('-');
        var test = checkRut(result[0],result[1]);
        if(test == 1){
            $.ajax({
                url: '<?= base_url() ?>Welcome/register',
                type: 'POST',
                data: $(this).serialize(),
                dataType: 'JSON'
            }).done(function (data) {
                if(data.status){
                    $('.tbodyRegistro').html('');
                    $('.tbodyRegistro').html(data.message);
                    console.log('Ogey');
                }else{
                    Swal.fire('Ocurrio un Problema',data.message,'error');
                    console.log('RRAT');
                }
            }).fail(function (data) {
                console.log(data);
            });
        }else{
            Swal.fire('Ocurrio un Problema','Rut Invalido, Favor verificar','error');
            console.log('OMEGARRAT');
        }
        
    });
    
    $(document).on('submit','.formPassword',function(event){
        event.preventDefault();
        $.ajax({
            url: '<?= base_url() ?>Welcome/actualizaPass',
            type: 'POST',
            data: $(this).serialize(),
            dataType: 'JSON'
        }).done(function (data) {
            if(data.status){
                $('.tbodyRegistro').html('');
                $('.tbodyRegistro').html(data.message);
                console.log('Ogey');
            }else{
                Swal.fire(
                    'Ocurrio un Problema',
                    data.message,
                    'error'
                );
                console.log('RRAT');
            }
        }).fail(function (data) {
            console.log(data);
        });
    });
    
    $(document).on('click','.iniciarSesion',function(event){
        event.preventDefault();
        window.location.href = '<?= base_url(); ?>';
    });
    
    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test($email);
    }
    
    function checkRut(rut,digito)
    {
        var cuerpo = rut.replace('.','');
        cuerpo = cuerpo.replace(',','');
        var dv = digito.toUpperCase();
        var valor = cuerpo + dv;
        rut = cuerpo + '-' + dv;

        suma = 0;
        multiplo = 2;
        console.log(cuerpo.length);
        for(i=1;i<=cuerpo.length;i++)
        {
            // Obtener su Producto con el Múltiplo Correspondiente
            index = multiplo * valor.charAt(cuerpo.length - i);

            // Sumar al Contador General
            suma = suma + index;

            // Consolidar Múltiplo dentro del rango [2,7]
            if(multiplo < 7)
            {
                multiplo = multiplo + 1;
            }
            else
            {
                multiplo = 2;
            }
    //        console.log(index);
    //        console.log('Char At '+valor.charAt(cuerpo.length - i));
    //        console.log('busqueda');
        }
        // Calcular Dígito Verificador en base al Módulo 11
        dvEsperado = 11 - (suma % 11);
        console.log('DV esperado ' + dvEsperado);
        // Casos Especiales (0 y K)
        dv = (dv == 'K')?10:dv;
        dv = (dv == 0)?11:dv;

        if(dvEsperado != dv)
        {
            return 0;
        }
        else
        {
            return 1;
        }

    }
</script>
<!--===============================================================================================-->
<script src="<?= base_url() ?>ext/js/main.js"></script>

</body>
</html>