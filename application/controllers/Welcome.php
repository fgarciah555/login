<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
    
    function __construct(){
        parent::__construct();
        $this->load->model('LoginModel');
    }

    public function index(){
        $this->load->view('admin/header');
        $this->load->view('login/login');
        $this->load->view('admin/footer');
    }
    
    public function registro(){   
        $this->load->view('admin/header');
        $this->load->view('login/register');
        $this->load->view('admin/footer');
    }
    
    public function login(){   
        $this->validaDatos($this->input->post());
        $user = $this->LoginModel->buscaUsuario($this->input->post('cod_user'));
        $this->validaUsuario($this->input->post(),$user);
    }
    
    public function register(){
        $this->validaDatosRegistro($this->input->post());
        $user = $this->LoginModel->buscaUsuarioMail($this->input->post('email'),$this->input->post('cod_user'));
        if($user){
            if($user['email'] == $this->input->post('email')){
                echo json_encode(array('status' => false,'message' => 'Ya existe una cuenta asociada a este correo electronico.'));
                exit();
            }
            if($user['cod_user'] == $this->input->post('cod_user')){
                echo json_encode(array('status' => false,'message' => 'La cuenta disponible esta en uso.'));
                exit();
            }
        }else{
            $password = $this->generaPass();
            $data_['email'] = $this->input->post('email');
            $data_['telefono'] = $this->input->post('telefono');
            $this->LoginModel->insertUser(array('cod_user' => $this->input->post('cod_user'),'email' => $this->input->post('email'),'telefono' => $this->input->post('telefono'),'rut' => $this->input->post('rut'),'password' => $password));
            $this->sendMail_($this->input->post('email'),$this->input->post('cod_user'),$password);
            $data = $this->load->view('login/tbodyRegister',$data_,true);
            echo json_encode(array('status' => true, 'message' => $data));
            exit();
        }
    }
    
    function generaPass(){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
        $randomString = '';
        for ($i = 0; $i < 10; $i++){ 
            $index = rand(0,strlen($characters) - 1); 
            $randomString .= $characters[$index]; 
        } 
        return $randomString; 
    }
    
    public function validaDatosRegistro($data){
        if($data['cod_user'] == '' && $data['email'] == '' && $data['telefono'] == ''){
            echo json_encode(array('status' => false,'message' => 'Debe completar los datos de Usuario, Email y Telefono'));
            exit();
        }if($data['cod_user'] == ''){
            echo json_encode(array('status' => false,'message' => 'Debe completar los datos de Usuario'));
            exit();
        }if($data['email'] == ''){
            echo json_encode(array('status' => false,'message' => 'Debe completar los datos de Email'));
            exit();
        }if($data['telefono'] == ''){
            echo json_encode(array('status' => false,'message' => 'Debe completar los datos de Telefono'));
            exit();
        }
    }
    
    public function validaDatos($data){
        if($data['cod_user'] == '' && $data['password'] == ''){
            echo json_encode(array('status' => false,'message' => 'Debe completar los datos de Usuario y Password'));
            exit();
        }if($data['cod_user'] == ''){
            echo json_encode(array('status' => false,'message' => 'Debe completar los datos de Usuario'));
            exit();
        }if($data['password'] == ''){
            echo json_encode(array('status' => false,'message' => 'Debe completar los datos de Password'));
            exit();
        }
    }
    
    public function validaUsuario($data,$user){
        if(!$user){
            echo json_encode(array('status' => false,'message' => 'No existe el usuario indicado'));
            exit();
        }
        if($data['cod_user'] == $user['cod_user']){
            if($data['password'] == $user['password']){
                echo json_encode(array('status' => true));
                exit();
            }else{
                echo json_encode(array('status' => false,'message' => 'La contraseña es incorrecta'));
                exit();
            }
        }
    }
    
    public function recuperaPass(){
        $this->load->view('admin/header');
        $this->load->view('login/password');
        $this->load->view('admin/footer');
    }
    
    public function actualizaPass(){
        $this->validaDatosPass($this->input->post());
        $user = $this->LoginModel->buscaMail($this->input->post('email'));
        if($user){
            $pass = $this->generaPass();
            $this->LoginModel->actualizaUser($this->input->post('email'),array('password' => $pass));
            $data_['email'] = $this->input->post('email');
            $data_['telefono'] = $user['telefono'];
            $this->sendMail_($this->input->post('email'),$user['cod_user'],$pass);
            $data = $this->load->view('login/tbodyPassword',$data_,true);
            echo json_encode(array('status' => true, 'message' => $data));
            exit();
        }else{
            echo json_encode(array('status' => false,'message' => 'No existe una cuenta asociada a este correo'));
        }
    }
    
    public function validaDatosPass($data){
        if($data['email'] == ''){
            echo json_encode(array('status' => false,'message' => 'Debe completar el dato de Correo electronico'));
            exit();
        }
    }
    
    function sendMail($email){
        $this->load->library('email');

        $this->email->from('test@test.cl', 'Pruebas Mail');
        $this->email->to($email);
//        $this->email->cc('fgarciah555@gmail.com');
//        $this->email->bcc('them@their-example.com');

        $this->email->subject('Email Test');
        $this->email->message('Testing the email class.');

        $this->email->send();
    }
    
    function sendMail_($email,$user,$pass){
        $mensajeCorreos['asunto'] = 'Asunto';
        $mensajeCorreos['header'] = 'Estas son sus credenciales de Acceso.';
        $mensajeCorreos['body'] = 'Body';
        $mensajeCorreos['footer'] = 'Saludos';
        
        $título = $mensajeCorreos['asunto'];

        // mensaje
        $cadena = '
        <html>
        <head>
          <title>Informacion Claro</title>
        </head>
        <body>
            <p>
                Estimado(a),
                <br><br>'.
                'Bienvenido a Plan Vital!'.
                '<br>'.
                $mensajeCorreos['header'].
                'Usuario: '. $user.
                '<br>'.
                'Password: '. $pass.
                '<br><br>'.
                $mensajeCorreos['footer'].
            '</p>
        </body>
        </html>
        ';
        $mensaje = wordwrap($cadena, 70, "\r\n");
        // Para enviar un correo HTML, debe establecerse la cabecera Content-type
        $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
        $cabeceras .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

        // Cabeceras adicionales
//        $cabeceras .= 'To: '.$nombre.' <'.$correo.'>' . "\r\n";
        $cabeceras .= 'From: Pruebas Wi-Fi <pruebalogin@prueba.cl>' . "\r\n";
        $cabeceras .= 'Cc: fgarciah555@gmail.com' . "\r\n";
        //$cabeceras .= 'Bcc: birthdaycheck@example.com' . "\r\n";

        // Enviarlo
        mail($email, $título, $mensaje, $cabeceras);
    }
}
