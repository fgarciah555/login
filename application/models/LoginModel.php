<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LoginModel extends CI_Model 
{

    protected $db;

    function __construct() 
    {
        parent::__construct();
        $this->local = $this->load->database('default', TRUE);
    }
    
    function buscaUsuario($data){
        $this->local->select('*');
        $this->local->where('cod_user',$data);
        $this->local->from('usuarios');
        $query = $this->local->get();
        if ($query->num_rows()){
            return $query->result_array()[0];
        }else{
            return false;
        }
    }
    
    function buscaUsuarioMail($email,$codUser){
        $this->local->select('*');
        $this->local->where('email',$email);
        $this->local->or_where('cod_user',$codUser);
        $this->local->from('usuarios');
        $query = $this->local->get();
        if ($query->num_rows()){
            return $query->result_array()[0];
        }else{
            return false;
        }
    }
    
    function buscaMail($email){
        $this->local->select('*');
        $this->local->where('email',$email);
        $this->local->from('usuarios');
        $query = $this->local->get();
        if ($query->num_rows()){
            return $query->result_array()[0];
        }else{
            return false;
        }
    }
    
    function insertUser($insert){
        $this->local->insert('usuarios',$insert);
    }
    
    function actualizaUser($email,$data){
        $this->local->where('email',$email);
        $this->local->update('usuarios',$data);
    }
}